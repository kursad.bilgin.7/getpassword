package main

import (
	"fmt"
	"math/rand"
	"time"
)

type KeyType struct {
	BasicKey []string
	HardKey  []string
}

func main() {
	var key KeyType
	key.BasicKey = append(key.BasicKey, "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNPQRSTUVWXYZ")
	key.HardKey = append(key.HardKey, "abcdefghijklmnopqrstuvwxyz0123456789.?/#*-;,!+%&{(=)}:><$ABCDEFGHIJKLMNPQRSTUVWXYZ")
	choice, length := Input()
	pass := Pass(choice, length, key)

	fmt.Println("Password ->", pass)
}

func Input() (int, int) {
	var choice, length int
	fmt.Print("select password difficulty basic(1), hard(2) -> ")
	fmt.Scan(&choice)
	fmt.Print("enter password length -> ")
	fmt.Scan(&length)
	return  choice, length
}

func Pass(choice, length int, key KeyType) string {
	var pass string
	r := rand.New(rand.NewSource(time.Now().Unix()))
	if choice == 1 {
		for i := 0; i < length; i++ {
			randIndex := r.Intn(len(key.BasicKey[0]))
			pass += string(key.BasicKey[0][randIndex])
		}
	} else if choice == 2 {
		for i := 0; i < length; i++ {
			randIndex := r.Intn(len(key.HardKey[0]))
			pass += string(key.HardKey[0][randIndex])
		}
	} else {
		fmt.Println("wrong difficulty level")
		main()
	}

	return pass
}